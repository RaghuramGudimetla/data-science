# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:36:50 2020

@author: gudim
"""

import pylab
from numpy import array, linalg, random, inf

def unitNormGraph(p):
 for i in range(5000):
  x = array([random.rand()*2-1,random.rand()*2-1])
  if ((x/linalg.norm(x,p)) == 1):
   pylab.plot(x[0],x[1],'bo')
pylab.axis([-1.5, 1.5, -1.5, 1.5])
pylab.show()
 
unitNormGraph(1)