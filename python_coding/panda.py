# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 16:17:32 2020

@author: gudim
"""
import pandas as pd

"""
data = pd.DataFrame({'Country': ['Russia','Colombia','Chile','Equador','Nigeria'],
                    'Rank':[121,40,100,130,11]})

desc = data.describe()

data.sort_values(by=['Rank'], inplace=True)
"""

data = pd.DataFrame({'k1':['one']*3 + ['two']*4, 'k2':[3,2,1,3,3,4,4]})

data.sort_values(by=['k1'], inplace=True)

data1 = data.drop_duplicates()

