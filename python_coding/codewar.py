# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 17:03:34 2020

@author: gudim
"""

def high_and_low(numbers):

    spl = numbers.split(" ")
    
    high = int(spl[0])
    
    low = int(spl[0])
        
    for val in spl:
        
        if(int(val) >= high):
            high = int(val)
            
        if(int(val) < low):
            low = int(val)
    
    numbers = str(high) + " " + str(low)
    
    return numbers

def DNA_strand(dna):

    ndna = ""
    
    for c in dna:
        
        if(c == 'A'):
            ndna += 'T'
        if(c == 'T'):
            ndna += 'A'    
        if(c == 'C'):
            ndna += 'G'
        if(c == 'G'):
            ndna += 'C'
    
    dna = ndna
    
    return dna
    