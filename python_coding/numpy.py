# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 14:20:38 2020

@author: gudim
"""

import numpy as np

L = list(range(30))

a = [str(a) for a in L]

#Creating arrays
'''
npArr = np.zeros(10, dtype='int')

npArr1 = np.ones((3,5), dtype='float')

npArr2 = np.full((5,5), 3, dtype='int')

npArr3 = np.arange(0, 30, 2)

npArr4 = np.linspace(0, 1, 50)

npArr5 = np.random.normal( 0, 1, (4,4))

npArr6 = np.eye(3)

npArr7 = np.random.randint(20, 40, size=(50,50))


#Accessing

arrVal = npArr7[0,1]
'''
#Concatination

arr1 = np.array((1,2,3))
arr2 = np.array((4,5,6))

arr3 = np.concatenate((arr1, arr2))

arr4 = np.array(((1,2,3),(4,5,6)))

arr5 = np.concatenate([arr4, arr4])

arr6 = np.concatenate([arr4, arr4], axis = 1)

arr7 = np.vstack([arr4, arr1])

arr8 = np.array(([3], [4]))

arr9 = np.hstack([arr4, arr8])
