# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 09:19:57 2020

@author: gudim
"""

#importing libraries

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#import train data
#RNN doesnt have any test set
#Only numpy arrays can be as inputs for RNN

data_train = pd.read_csv('Google_Stock_Price_Train.csv')

training_set = data_train.iloc[:, 1:2].values

#Feature Scaling

from sklearn.preprocessing import MinMaxScaler

sc = MinMaxScaler( feature_range= (0, 1) )

training_set_scaled = sc.fit_transform(training_set)

#Creating a data structure with 60 timesteps and 1 output
#This means, neural network at a particular position will check for the 60 previous scores
#We will be making the previous values as numpy arrays

x_train = []
y_train = []

for i in range(60, 1258):
    x_train.append(training_set_scaled[i-60:i, 0])
    y_train.append(training_set_scaled[i, 0])
    
x_train, y_train = np.array(x_train), np.array(y_train)    


#Reshaping the data into 3D for RNN to run

x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))


#Import all important keras libraries

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout

#Initialize RNN

regressor = Sequential()

#Adding of LSTM layers to this

regressor.add(LSTM( units=50, return_sequences= True, input_shape = (x_train.shape[1], 1)))
regressor.add(Dropout(rate = 0.2))

regressor.add(LSTM( units=50, return_sequences= True ))
regressor.add(Dropout(rate = 0.2))

regressor.add(LSTM( units=50, return_sequences= True ))
regressor.add(Dropout(rate = 0.2))

regressor.add(LSTM( units=50 ))
regressor.add(Dropout(rate = 0.2))

regressor.add(Dense(units = 1))

#Compile the regressor
regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

#Compiled regressor needs to get fitted now

regressor.fit( x_train, y_train, epochs = 100, batch_size = 32)


#Getting the real tesr data set

data_test = pd.read_csv('Google_Stock_Price_Test.csv')

real_stock_price = data_test.iloc[:, 1:2].values