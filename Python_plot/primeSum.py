# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 12:56:29 2020

@author: Gudimetla Raghuram
"""

import numpy as np

primeList = []

sum = 0

for val in range(2,2000000+1):
    
  sqrRootNumber= (np.sqrt(val).astype(np.int64)) + 1

  for i in range(2,sqrRootNumber):

    if ( val % i == 0 ):
      break

  else:
    primeList.append(val)

for i in primeList:
    
    sum = sum + i
    
print(sum)    