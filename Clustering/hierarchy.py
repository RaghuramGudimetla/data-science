# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 15:28:48 2020

@author: gudim
"""

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler
import scipy.cluster.hierarchy as sch
from sklearn.cluster import AgglomerativeClustering

dataset = pd.read_csv('Mall_Customers.csv')
X = dataset.iloc[:,[3,4]].values

sc = StandardScaler()
X = sc.fit_transform(X)

dendogram = sch.dendrogram(sch.linkage(X, method='ward'))
plt.title('Dendogram')
plt.xlabel('Customers')
plt.ylabel('Euclidean distances')
plt.show()

hc = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='ward')

y_pred = hc.fit_predict(X)

plt.scatter(X[y_pred == 0, 0], X[y_pred == 0,  1], s = 100, c = 'red', label = 'Clu 1')
plt.scatter(X[y_pred == 1, 0], X[y_pred == 1,  1], s = 100, c = 'blue', label = 'Clu 1')
plt.scatter(X[y_pred == 2, 0], X[y_pred == 2,  1], s = 100, c = 'green', label = 'Clu 1')
plt.scatter(X[y_pred == 3, 0], X[y_pred == 3,  1], s = 100, c = 'yellow', label = 'Clu 1')
plt.scatter(X[y_pred == 4, 0], X[y_pred == 4,  1], s = 100, c = 'black', label = 'Clu 1')
plt.title('Clusters using hierarchial clustering')
plt.xlabel('X - Value')
plt.ylabel('Y - Value')
plt.legend()
plt.show()