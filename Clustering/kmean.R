

dataset <- read.csv("Mall_Customers.csv")

data <- dataset[, 4:5]

#Select the optimal number of clusters
set.seed(6)
wcss <- vector()

for(i in 1:10) {
  
    wcss[i] <- sum(kmeans(data,i)$withinss)
  
}

plot(1:10, wcss, type = "b")

#Get the kmeans create and apply to the dataset

kmeans <- kmeans(data, 5, iter.max = 300, nstart = 10)

library(cluster)

clusplot(data,
         kmeans$cluster,
         lines = 0,
         shade = TRUE,
         color = TRUE,
         labels = 2,
         plotchar = FALSE,
         span = TRUE,
         main = "Just",
         xlab = "X - axis",
         ylab = "Y - axis")