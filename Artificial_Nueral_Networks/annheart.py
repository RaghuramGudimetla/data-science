# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 01:29:10 2020

@author: gudim
"""

import pandas as pd
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier

dataset = pd.read_csv('heart.csv')

x = dataset.iloc[:, 0:12].values
y = dataset.iloc[:, 13].values

X_train, X_test, y_train, y_test = train_test_split(x,y, test_size = 0.3, random_state = 42)

sc=StandardScaler()

X_train = sc.fit_transform(X_train)
X_test = sc.fit_transform(X_test)

classifier = Sequential()

classifier.add(Dense(activation='relu', input_dim= 12, units = 18, kernel_initializer='uniform'))
classifier.add(Dropout(0.2))

classifier.add(Dense(activation='relu', units = 24, kernel_initializer='uniform'))
classifier.add(Dropout(0.2))

classifier.add(Dense(activation='relu', units = 18, kernel_initializer='uniform'))
classifier.add(Dropout(0.2))

classifier.add(Dense(activation='relu', units = 12, kernel_initializer='uniform'))
classifier.add(Dropout(0.2))

classifier.add(Dense(activation='relu', units = 6, kernel_initializer='uniform'))
classifier.add(Dropout(0.2))

classifier.add(Dense(activation='sigmoid', units = 1, kernel_initializer='uniform'))

classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

classifier.fit( x = X_train, y = y_train, batch_size=30, epochs=30)

y_pred = classifier.predict(X_test)

y_pred = (y_pred > 0.5)

cm = confusion_matrix(y_test, y_pred)
ac = accuracy_score(y_test, y_pred)

#K - fold cross validation
def build_classifier():
    classifier = Sequential()
    classifier.add(Dense(activation='relu', input_dim= 12, units = 18, kernel_initializer='uniform'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(activation='relu', units = 24, kernel_initializer='uniform'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(activation='relu', units = 18, kernel_initializer='uniform'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(activation='relu', units = 12, kernel_initializer='uniform'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(activation='relu', units = 6, kernel_initializer='uniform'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(activation='sigmoid', units = 1, kernel_initializer='uniform'))
    classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return classifier

classifier = KerasClassifier( build_fn=build_classifier, nb_epoch=30, batch_size=30, verbose=0 )
accuracies = cross_val_score(classifier, X_train, y = y_train, scoring='accuracy', cv = 10, error_score='raise')
mean = accuracies.mean()
standard_deviation = accuracies.std()

#Grid Search
def build_classifier():
    classifier = Sequential()
    classifier.add(Dense(activation='relu', input_dim= 12, units = 18, kernel_initializer='uniform'))
    classifier.add(Dense(activation='relu', units = 24, kernel_initializer='uniform'))
    classifier.add(Dense(activation='relu', units = 18, kernel_initializer='uniform'))
    classifier.add(Dense(activation='relu', units = 12, kernel_initializer='uniform'))
    classifier.add(Dense(activation='relu', units = 6, kernel_initializer='uniform'))
    classifier.add(Dense(activation='sigmoid', units = 1, kernel_initializer='uniform'))
    classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return classifier

classifier = KerasClassifier( build_fn=build_classifier )

parameters = {'epochs': [20,30,50,100],
              'batch_size': [10,20,30,50,100]
              }

grid_s = GridSearchCV(classifier,
                      param_grid=parameters,
                      scoring='accuracy',
                      cv = 10
                      )

grid_s = grid_s.fit(X_train, y = y_train)

best_p = grid_s.best_params_
best_acc = grid_s.best_score_