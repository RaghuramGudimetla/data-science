# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 17:40:55 2020

@author: gudim
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# Encoding categorical data

labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#Import sequential module that helps initilializing the neural network
#Import dense module to build the layers of deep learning model

#Initialize the neural network using Sequential model
classifier = Sequential()

#Creating the multiple layers in neural network
classifier.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))

classifier.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))

classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))

#Now we have to compile the ANN with optimizer, loss and metrics function

classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

classifier.fit( x = X_train, y = y_train, batch_size = 10, epochs = 50)

accuracies = cross_val_score(estimator=classifier, scoring='accuracy', X=X_train, y=y_train, cv = 3)

#Predict the test data
y_pred = classifier.predict(X_test)
y_pred = ( y_pred > 0.5 )

#Get the confusin matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix( y_test, y_pred )

#Now evaluating and customizing the Artificial Neural Networks

#Evaluating

def build_classifier():
    classifier1 = Sequential()
    classifier1.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))
    classifier1.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))
    classifier1.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))
    classifier1.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
    return classifier

classifier1 = KerasClassifier( build_fn=build_classifier, nb_epoch=150, batch_size=10, verbose=0 )
accuracies = cross_val_score( estimator = classifier1, X = X_train, y = y_train, scoring = 'accuracy', cv = 10, n_jobs = -1)
mean = accuracies.mean()

standard_deviation = accuracies.std()
