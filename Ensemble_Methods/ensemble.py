# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 09:47:03 2020

@author: gudim
"""

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, BaggingClassifier, AdaBoostClassifier, VotingClassifier
from sklearn.preprocessing import StandardScaler

dataset = pd.read_csv('Social_Network_Ads.csv')

X = dataset.iloc[:, [2, 3]].values
y = dataset.iloc[:, 4].values

# Splitting the dataset into the Training set and Test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

'''
decisionClassifier = DecisionTreeClassifier()
decisionClassifier.fit(X_train, y_train)

decisionClassifier = RandomForestClassifier(n_estimators=10)
decisionClassifier.fit(X_train, y_train)

score = decisionClassifier.score(X_test, y_test)

train_Score = decisionClassifier.score(X_train, y_train) #this is 1.0 means over fitting

#Bagging classifier

baggingClass = BaggingClassifier(DecisionTreeClassifier(), max_samples=0.5, max_features=1.0, n_estimators=20)
baggingClass.fit(X_train, y_train)

train_bagging_score = baggingClass.score(X_train,y_train)
test_bagging_score = baggingClass.score(X_test, y_test)

boostClass = AdaBoostClassifier(LogisticRegression(), n_estimators=10, learning_rate=1)
boostClass.fit(X_train, y_train)

bst = boostClass.score(X_train, y_train)

bstest = boostClass.score(X_test, y_test)

#Checking boosting with multiple classifiers can help us driving the best results

'''
lr = LogisticRegression()
dt = DecisionTreeClassifier()
svm = SVC(kernel = 'poly', degree = 2)

votClass = VotingClassifier(estimators = [('lr', lr),('dt', dt),('svm', svm)], voting = 'hard')
votClass.fit(X_train, y_train)

trainScore = votClass.score(X_train, y_train)
testScore = votClass.score(X_test, y_test)

