#NLP
#Code Author :Swapna Josmi Sam

#Importing text data
dataSet = read.delim( 'Restaurant_Reviews.tsv', quote = '', stringsAsFactors = FALSE )

#Cleaning texts

#Install package (tm)

install.packages('tm')
install.packages('SnowBallC')

#Using corpus convert the data as require

library(tm)
library(SnowballC)

#Import the required data in corpus
corpus = VCorpus(VectorSource(dataSet$Review))

#Removing Capital and replacing with lower
corpus = tm_map(corpus, content_transformer(tolower))

#Removing Numbers
corpus = tm_map(corpus, removeNumbers)

#Remove Punctuations
corpus = tm_map(corpus, removePunctuation)

#Remove irrelevant words
corpus = tm_map(corpus, removeWords, stopwords())

#Now step the similar kind of words usinf stemDocument
corpus = tm_map(corpus, stemDocument)

#Remove white spaces
corpus = tm_map(corpus, stripWhitespace)

#Creating columns with NLP of data
dtm = DocumentTermMatrix(corpus)

#Remove very few remaining terms which cannot be useful and have no impact
dtm = removeSparseTerms(dtm, 0.999)

#Create Data set using dtm matrix like a data frame
dataset_new = as.data.frame(as.matrix(dtm))

#Now adding the dependent variable column to the data frame
dataset_new$Liked = dataSet$Liked

#Now perform Machine learning algorithm of Random Forest

dataset_new$Liked = factor(dataset_new$Liked, levels = c(0,1))

#Split data into training and testing
library(caTools)
set.seed(123)
split = sample.split(dataset_new$Liked, SplitRatio = 0.8)
training_set = subset(dataset_new, split == TRUE)
test_set = subset(dataset_new, split == FALSE)

#perform random forest algorithm
library(randomForest)
classifier = randomForest(x = training_set[-692], y = training_set$Liked, ntree = 50)

#Predicting the model
y_pred = predict( classifier, newdata = test_set[-692])

#Checking the accuracy using confusion matrix
cm = table(test_set[, 692], y_pred)
