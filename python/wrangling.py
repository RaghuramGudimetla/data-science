import pandas as pd

filename = '/home/raghuram/Desktop/dstest.CSV'

#reading the CSV file
df = pd.read_csv(filename)

#checking types
#print(df.dtypes)

#describing
#print(df.describe())

#group by output 
#df.groupby(by =['Description', 'payments']).size()

#Total missing values
#print(df.isna().sum())

#Remove empty rows
#df = df.dropna(axis = 0, how = 'any')
#print(df)

#Checking duplicate values
#print(df.nunique())

#Display duplicate rows
#print(df[df.duplicated(subset = 'Description', keep =False)].sort_values('Description'))

#How many individualt duplicates
repeat_patients = df.groupby(by = 'Description').size().sort_values(ascending =True)
print(repeat_patients)




