import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.naive_bayes import GaussianNB

#Read data in CSV file , You give your local path where you saved this CSV file
#Put that training excel in one path and give that path here
url = "./iris.csv"

# Assign colum names to the dataset
#Give names according to the excel i have given according to iris data
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# Read dataset to pandas dataframe
dataset = pd.read_csv(url, names=names)

t = zeros(len(target))
t[target == 'setosa'] = 1
t[target == 'versicolor'] = 2
t[target == 'virginica'] = 3

#This is your training data now
#print(dataset)

#In the question they asked to predict mpg(which is second column) based on weight(which is 6th column)
#Now you need to get these both values and put in Y(mpg) and X(Weight)
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values
# X = dataset.iloc[:, 1].values #1 gives you 1st row elements
# y = dataset.iloc[:, ].values #5 gives you 5th row elements

#Now variables X has mpg values and y has its corresponding weights

#Now divide your data into training set and testing sets

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)
#This step you need not do because your data is already divided and your college gave different sets for training and testing
#Rather than this step you use below to load your test data

#Now get the test data as well
#I am not using this below steps coz i divided my whole data into test and train in above step
#You should ignore above and do this

# url = "./testData.csv"
# dataset_test = pd.read_csv(url, names=names)

# X_test = dataset_test.iloc[:, 1].values
# y_test = dataset_test.iloc[:, 5].values

# X_train = X
# y_train = y

#This below step is not mandatory but we need to scale the data if we have a numerous different numbers
#Since the range of value varies better to scale
scaler = StandardScaler()
scaler.fit(X_train)

X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# Now we need to perform KNN algorithm on your training data as below
# Here n_neighbor is k da, So you need to change this
classifier = KNeighborsClassifier(n_neighbors=5)

# You need to use this algorithm and find the classifying for Y-which we need to predict(mpg) based on x-which is weight
# We do the classifying on training data for X_train(weight) and y_train(mpg)
classifier.fit(X_train, y_train)

# Once we get the claffifier, we need to predict this on test data as per your assignment
# Predict the Y(mpg) using X(weight)

y_predicted = classifier.predict(X_test)
# Using the classifier we got from train data, you predicted the Y(mpg) for the X(weights) in test data which is X_test

#print(y_predicted)

#print(confusion_matrix(y_test, y_predicted))
#print(classification_report(y_test, y_predicted))

classifier.fit(train,t_train) # train
print classifier.score(test,t_test) # test