#import urllib.request
from numpy import genfromtxt, zeros
from sklearn.naive_bayes import GaussianNB
from pylab import plot, show

# url = "http://aima.cs.berkeley.edu/data/iris.csv"
# request = urllib.request.Request(url)
# response = urllib.request.urlopen(request)
# localFile = open('iris.csv', 'w')
# localFile.write(response.read().decode('utf-8'))
# localFile.close()

# read the first 4 columns
data = genfromtxt('iris.csv',delimiter=',',usecols=(0,1,2,3)) 
# read the fifth column
target = genfromtxt('iris.csv',delimiter=',',usecols=(4),dtype=str)

#Get the shape like no of rows and colums of data selected
#print(data.shape)

#Get the shape like no of rows and colums of data selected
#print(target.shape)

#How many categories of the data is there
#print( set(target) )

# plot(data[target=='setosa',0],data[target=='setosa',2],'bo')
# plot(data[target=='versicolor',0],data[target=='versicolor',2],'ro')
# plot(data[target=='virginica',0],data[target=='virginica',2],'go')
# show()

# Converting vector strings into integers
t = zeros(len(target))
t[target == 'setosa'] = 1
t[target == 'versicolor'] = 2
t[target == 'virginica'] = 3

# Classification using Gaussian Naive Bayes
# classifier = GaussianNB()

# print(len(data))
print(t)

# classifier.fit(data,t) # training on the iris dataset

# print(t[0])
