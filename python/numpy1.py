import numpy as np

lis_a = [1,3,4]

#make a numpy array
#print(np.array(lis_a))

#with step size
#print(np.arange(0,11,2))

#Arrays of one and two dimension values
# print(np.ones(3))
# print(np.ones((3,4)))

#identity Metrix = Diagonal one and rest is zero (y * y)