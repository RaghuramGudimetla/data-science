#Usage of numpy
import numpy as np

#Creating array using numpy
arr = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
# print(arr)

#Creating 3 X 3 array
#arr = np.full((3, 3), True, dtype=bool)
# arr = np.ones((3,3), dtype=bool)
# print(arr)

#Odd numbers and Even numbers
# print(arr[ arr%2 == 0])
# print(arr[ arr%2 == 1])

#Replace values satisfying with single condition
# arr[ arr%2 == 1] = -10
# print(arr)

#Assign values to other array after replacing
#arr = np.arange(15)
# out = np.where(arr % 2 == 1, -1, arr)
# print(arr)
# print(out)

#Reshaping an array
# arr.reshape(3, -1) 
# print(arr.reshape(3, -1))

#Common in two arrays
a = np.array([1,2,3,2,3,4,3,4,5,6])
b = np.array([7,2,10,2,7,4,9,4,9,8])
print(np.intersect1d(a,b))